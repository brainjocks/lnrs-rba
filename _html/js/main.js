$(function () {

  // make body no scroll when menu is open
  $('.navbar-collapse').on('hidden.bs.collapse', function () {
    $('body').removeClass('menu-open');
  })
  $('.navbar-collapse').on('shown.bs.collapse', function () {
    $('body').addClass('menu-open');
  })

  $('.started-tiles .inner').matchHeight();
  $('.simple-tiles .inner').matchHeight();

});
